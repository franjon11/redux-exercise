import { useDispatch, useSelector } from 'react-redux';

import { actionNumber, actionDot} from 'state/actions/numberAction';
import {actionAdd, actionSub, actionMult, actionDiv, actionSqrt, actionSum, actionUndo, actionIntro } from 'state/actions/functionAction'
import { selectCurrentNumber } from 'state/selectors/selectCurrentNumber';
import { selectCurrentStack } from 'state/selectors/selectCurrentStack';

import styles from './Calculator.module.css';

const renderStackItem = (value: number, index: number) => {
  return <div key={index}>{value}</div>;
};

export const Calculator = () => {
  const currentNumber = useSelector(selectCurrentNumber);
  const stack = useSelector(selectCurrentStack);

  const dispatch = useDispatch();
  const onClickNumber = (n: number) => {
    const action = actionNumber(n);
    dispatch(action);
  };

  const resetNumber = () => {
    const reset = resetNumber();
    dispatch(reset)
  }

  const onClickAdd = () => {
    const action = actionAdd(currentNumber);
    dispatch(action);
    resetNumber();
  };
  const onClickSub = () => {
    const action = actionSub(currentNumber);
    dispatch(action);
    resetNumber();
  };
  const onClickMult = () => {
    const action = actionMult(currentNumber);
    dispatch(action);
    resetNumber();
  };
  const onClickDiv = () => {
    const action = actionDiv(currentNumber);
    dispatch(action);
    resetNumber();
  };
  const onClickSqrt = () => {
    const action = actionSqrt(currentNumber);
    dispatch(action);
    resetNumber();
  };
  const onClickSum = () => {
    const action = actionSum(currentNumber);
    dispatch(action);
    resetNumber();
  };
  const onClickUndo = () => {
    const action = actionUndo(1);
    dispatch(action);
  };
  const onClickIntro = () => {
    const action = actionIntro(currentNumber);
    dispatch(action);
    resetNumber();
  };
  const onClickDot = () => {
    const action = actionDot();
    dispatch(action);
  };

  return (
    <div className={styles.main}>
      <div className={styles.display}>{currentNumber}</div>
      <div className={styles.numberKeyContainer}>
        {[...Array(9).keys()].map((i) => (
          <button key={i} onClick={() => onClickNumber(i + 1)}>
            {i + 1}
          </button>
        ))}
        <button className={styles.zeroNumber} onClick={() => onClickNumber(0)}>
          0
        </button>
        <button onClick={() => onClickDot()}>.</button>
      </div>
      <div className={styles.opKeyContainer}>
        <button onClick={() => onClickAdd()}>+</button>
        <button onClick={() => onClickSub()}>-</button>
        <button onClick={() => onClickMult()}>x</button>
        <button onClick={() => onClickDiv()}>/</button>
        <button onClick={() => onClickSqrt()}>√</button>
        <button onClick={() => onClickSum()}>Σ</button>
        <button onClick={() => onClickUndo()}>Undo</button>
        <button onClick={() => onClickIntro()}>Intro</button>
      </div>
      <div className={styles.stack}>{stack.map(renderStackItem)}</div>
    </div>
  );
};
