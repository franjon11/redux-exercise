import { AppAction } from '../AppAction';
import { Reducer } from 'redux';

type StateStack = {
  actualStack: number[];
};

const initialState: StateStack = {
  actualStack: [],
};

export const functionReducer: Reducer<StateStack, AppAction> = (
  state = initialState,
  action
) => {
  if (action.type === 'FUNCTION') {
    switch (action.func) {
      case 'INTRO':
        return { actualStack: [...state.actualStack, action.n] };

      case 'ADD':
        if (state.actualStack.length == 0) return state;
        if (state.actualStack.length == 1 && action.n == 0) return state;

        const stackAdd: number[] = [...state.actualStack];
        pushNewAction(action, stackAdd)

        return { actualStack: [...stackAdd, Number(stackAdd.pop()) + Number(stackAdd.pop())] };

      case 'SUB':
        if (state.actualStack.length == 0) return state;
        if (state.actualStack.length == 1 && action.n == 0) return state;

        const stackSub: number[] = [...state.actualStack];
        pushNewAction(action, stackSub)

        return {actualStack: [...stackSub, Number(stackSub.pop()) - Number(stackSub.pop())]};

      case 'MULT':
        if (state.actualStack.length == 0) return state;
        if (state.actualStack.length == 1 && action.n == 0) return state;

        const stackMult: number[] = [...state.actualStack];
        pushNewAction(action, stackMult)
        

        return {actualStack: [...stackMult, Number(stackMult.pop()) * Number(stackMult.pop())]};

      case 'DIV':
        if (state.actualStack.length == 0) return state;
        if (state.actualStack.length == 1 && action.n == 0) return state;

        const stackDiv: number[] = [...state.actualStack];
        pushNewAction(action, stackDiv)

        const up: number = Number(stackDiv.pop());
        const down: number = Number(stackDiv.pop());
        if (down == 0) {
          return {actualStack:[...stackDiv, down, up]}
        }
        return { actualStack: [...stackDiv, up/down]}

      case 'SQRT':
        if (state.actualStack.length == 0 && action.n == 0) return state;

        const stackSqrt: number[] = state.actualStack;
        pushNewAction(action, stackSqrt)
    
        const num: number = Number(stackSqrt.pop());

        if (num < 0) {
            stackSqrt.push(num);
            return { actualStack: stackSqrt }
        }

        return {actualStack: [...stackSqrt, Math.sqrt(num)]};

      case 'SUM':
        const stackSum: number[] = state.actualStack;
        pushNewAction(action, stackSum)
    
        if (stackSum.length == 0) return state;

        return {actualStack: [stackSum.reduce((x,y) => x+y)]}

      default:
        return state
    }
  } else {
    return state;
  }
};

const pushNewAction = (action: AppAction, stackAux: number[]) => {
    if (action.n != 0) stackAux.push(action.n);
}