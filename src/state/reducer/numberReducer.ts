import { Reducer } from 'redux';
import { AppAction } from '../AppAction';

type StateNumber = {
  valor: number;
  decimales: number;
  tieneDecimales: boolean;
};

const initialState: StateNumber = {
  valor: 0,
  decimales: 0,
  tieneDecimales: false,
};

export const numberReducer: Reducer<StateNumber, AppAction> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case 'NUMBER':
      const newDecimals: number = state.tieneDecimales ? state.decimales+1 : state.decimales
      const mult: number = state.tieneDecimales ? 1/Math.pow(10, newDecimals) : 10
      return state.tieneDecimales ?
          { valor: state.valor + action.n * mult, decimales: newDecimals, tieneDecimales: state.tieneDecimales }
          :
          { valor: state.valor * mult + action.n, decimales: newDecimals, tieneDecimales: state.tieneDecimales };

    case 'DOT':
      return state.tieneDecimales ? state : {valor: state.valor, decimales: state.decimales, tieneDecimales: true }
    case 'RESET':
      return { valor: 0, decimales: 0, tieneDecimales: false }

    default:
      return state;
  }
};