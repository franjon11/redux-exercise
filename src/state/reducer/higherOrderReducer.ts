import { AnyAction, Reducer } from 'redux';
import { AppAction } from 'state';

type HigherOrderState<S> = {
  past: S[];
  content: S;
  actionCount: number;
};

function getInitialState<S>(reducer: Reducer<S, AnyAction>): S {
  return reducer(undefined, { type: undefined });
}

export function higherOrderReducer<S>(
  reducer: Reducer<S, AppAction>
): Reducer<HigherOrderState<S>, AppAction> {
  const initialState = {
    past: [],
    content: getInitialState(reducer),
    actionCount: 0,
  };
  return (state = initialState, action) => {
    const {past, content, actionCount} = state
    if (action.type === 'FUNCTION') {
      if (action.func === 'UNDO') {
        const previous = past[past.length - 1];
        const newPast: S[] = past.slice(0, past.length - 1);
        return {
          past: newPast,
          content: previous,
          actionCount: actionCount - 1
        }
      }
      else{
        const newContent: S = reducer(state.content, action)
        if (newContent === content) { return state }
        return {
          past: [...past, content],
          content: newContent,
          actionCount: state.actionCount + 1,
        };
      }
    } else {
      const newContent: S = reducer(state.content, action)
      if (newContent === content) { return state }
      return {
        past: [...past, content],
        content: newContent,
        actionCount: state.actionCount + 1,
      };
    }
  };
}
