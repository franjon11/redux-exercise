import { combineReducers } from 'redux';

import { higherOrderReducer } from './higherOrderReducer';
import { numberReducer } from './numberReducer';
import { functionReducer } from './functionReducer';

export const rootReducer = higherOrderReducer(
  combineReducers({
    numberSample: numberReducer,
    functionSample: functionReducer,
  })
);
