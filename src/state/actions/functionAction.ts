type FunctionAction = {
    type: 'FUNCTION';
    func: 'ADD' | 'SUB' | 'MULT' | 'DIV' | 'SQRT' | 'SUM' | 'UNDO'| 'INTRO';
    n: number;
  };

  export const actionAdd = (num: number): FunctionAction => ({
    type: 'FUNCTION',
    func: 'ADD',
    n: num,
  });

  export const actionSub = (num: number): FunctionAction => ({
    type: 'FUNCTION',
    func: 'SUB',
    n: num,
  });

  export const actionMult = (num: number): FunctionAction => ({
    type: 'FUNCTION',
    func: 'MULT',
    n: num,
  });

  export const actionDiv = (num: number): FunctionAction => ({
    type: 'FUNCTION',
    func: 'DIV',
    n: num,
  });

  export const actionSqrt = (num: number): FunctionAction => ({
    type: 'FUNCTION',
    func: 'SQRT',
    n: num,
  });

  export const actionSum = (num: number): FunctionAction => ({
    type: 'FUNCTION',
    func: 'SUM',
    n: num,
  });

  export const actionUndo = (num: number): FunctionAction => ({
    type: 'FUNCTION',
    func: 'UNDO',
    n: num,
  });

  export const actionIntro = (num: number): FunctionAction => ({
    type: 'FUNCTION',
    func: 'INTRO',
    n: num,
  });