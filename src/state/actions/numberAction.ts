type NumberAction = {
    type: 'NUMBER' | 'DOT' | 'RESET';
    func: string;
    n: number;
  };
  
  export const actionNumber = (num: number): NumberAction => ({
    type: 'NUMBER',
    func: "",
    n: num,
  });

  export const actionDot = (): NumberAction => ({
    type: 'DOT',
    func: "",
    n: 0,
  });

  export const actionReset = (): NumberAction => ({
    type: 'RESET',
    func: "",
    n: 0,
  });
